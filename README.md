# Задание
## 1. Docker + GitlabCI
Взять любой open-source проект на php (например, https://github.com/aimeos/aimeos-laravel), форкнуть его, поднять 
на своем сервере, упаковать в docker и сделать gitlab-ci пайплайн, так, чтобы можно было сделать коммит и посмотреть, 
что изменения приехали на сервер (личный компьютер, ноутбук - при отсутствии удаленного) автоматически.
### Примечания:
Для выполнения задания необходимо наличие VPS, если с этим проблемы - можно обратиться к нам, мы предоставим песочницу.
Gitlab можно использовать облачный, а можно развернуть standalone-версию на этом же VPS.
Для оценки задания необходимо предоставить доступ в репозиторий с приложением с правами на коммит в master.
### Хорошо если:
В пайплайне будет стадия релиза на которой формируется changelog.

## 2. Netdata plugin
Написать плагин (custom collector) для open-source системы мониторинга Netdata, который проверяет состояние здоровья 
удаленного приложения и выводит эту информацию.
### Примечания:
Проверить состояние приложения - https://api-dev.zveno.io/health 
Можно использовать любой подходящий ЯП - go, python.
Для оценки тестового задания предоставить ссылку на репозиторий с кодом плагина.


# Последовательность запуска сервиса

1. Создаем Vagrantfile для сборки приложения. 

2. скачать компосер `wget https://getcomposer.org/download/latest-stable/composer.phar -O composer`

3. Установить репозиторий с php8.0 `sudo add-apt-repository ppa:ondrej/php && sudo apt-get update`

4. Установить php со всеми необходимыми компонентами `sudo apt install php8.0 php8.0-curl php8.0-xml php8.0-gd php8.0-intl php8.0-zip php8.0-mysql`

5. Установим mysql `sudo apt install mysql-server`

6. Подключаемся через `sudo mysql` и меняем root пароль `ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password by 'mysqlpassword';`

7. Создаем базу данных `CREATE DATABASE laravel;`

8. Запустить composer `php composer create-project aimeos/aimeos /sgmk-test/myshop` логин:пароль admin:admin

9. дописать в ./myshop/composer.json:
```json
    "prefer-stable": true,
    "minimum-stability": "dev",
    "require": {
        "aimeos/aimeos-laravel": "~2022.04",
        ...
    },
    "scripts": {
        "post-update-cmd": [
            "@php artisan migrate",
            "@php artisan vendor:publish --tag=public --force",
            "\\Aimeos\\Shop\\Composer::join"
        ],
        ...
    }
```

10. запустить php composer update

11. запустить `cd /sgmk-test/myshop/ && php artisan vendor:publish --all && php artisan migrate && php artisan aimeos:setup`

12. Запустить вебсервер `php artisan serve` для проверки

13. Снимаем дамп базы данных для дальнейшего использования при развертывании с нуля.
`sudo mysqldump -p laravel > /sgmk-test/mysql/init_db.sql`  
(возможно придется скорректировать, в дампе не указано то что надо создать базу данных с именем laravel)

14. Создаем докер-композ

15. Создаем докер-файл

16. меняем ./myshop/.env
```
APP_ENV=development
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=mysqlusername
DB_PASSWORD=mysqlpassword
```

17. Подготавливаем доменное имя и сервер. 

18. прописать в env APP_ENV=production и APP_URL=http://sai.ekograd.su:80 ASSET_URL=http://sai.ekograd.su:80

19. Подготовить .env.example и настроить инициализирующий скрипт init.sh для копирования переменных
20. Настраиваем Gitlab runner
```shell
sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
sudo chmod +x /usr/local/bin/gitlab-runner
sudo gitlab-runner start
```
21. регистрируем раннер

22. скорректировать uid gid в .env на раннера (в нашем случае 113:117)

23. Добавим контейнер с netdata в docker-compose.yml

